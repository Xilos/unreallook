﻿using UnityEngine;

public class airbus : MonoBehaviour {
    float start;
    public GameObject closeButton;

	// Use this for initialization
	void Awake () {
        start = Time.time;
	}

    // Update is called once per frame
    void Update()
    {
        if (closeButton.activeSelf == false)
        {
            if (Time.time - start >= 2)
            {
                transform.parent = null;
                closeButton.SetActive(true);
            }
        }
    }

    public void closeIt()
    {
        Camera.main.transform.position = new Vector3(0, 0, 0);
        Camera.main.transform.rotation = new Quaternion(0, 0, 0, 0);
        Destroy(gameObject);
    }
}
