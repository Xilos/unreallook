﻿using UnityEngine;

public class instruction : MonoBehaviour
{
    private int cur = 1;
    // Start is called before the first frame update
    void Start()
    {
        //PlayerPrefs.SetInt("saw_instruction", 0);
    }

    public void next()
    {
        switch (cur)
        {
            case 1:
                GetComponent<Animator>().Play("second_appear");
                cur++;
                break;
            case 2:
                GetComponent<Animator>().Play("third_appear");
                cur++;
                break;
            case 3:
                PlayerPrefs.SetInt("saw_instruction", 1);
                Destroy(gameObject);
                break;
            default:
                return;
        }
    }

    public void back()
    {
        switch (cur)
        {
            case 2:
                GetComponent<Animator>().Play("first_appear");
                cur--;
                break;
            case 3:
                GetComponent<Animator>().Play("second_appear");
                cur--;
                break;
            default:
                return;
        }
    }
}
