﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    /*
    private void Awake()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }*/

    public void Shop()
    {
        Application.OpenURL("https://unreallook.ru/");
    }

    public void Rate()
    {
#if UNITY_ANDROID
        Application.OpenURL("https://play.google.com/store/apps/details?id=com.Phygitality.UnrealLook");
#endif
#if UNITY_EDITOR
        Application.OpenURL("https://unreallook.ru/");
#endif
#if UNITY_IOS
        Application.OpenURL("https://unreallook.ru/");
#endif
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void toVuforia()
    {
        SceneManager.LoadScene("vuforia");
    }
}
