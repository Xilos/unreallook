﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ARbuttons : MonoBehaviour
{
    public GameObject[] hide, show;
    public GameObject screensaved;
    public GameObject instruction;

    private void Awake()
    {
        int saw = PlayerPrefs.GetInt("saw_instruction", 0);
        if (saw == 0) openInstruction();
        Debug.Log(System.DateTime.Now.ToString("yy.MM.dd HH:mm:ss"));
    }

    public void toMenu()
    {
        SceneManager.LoadScene("menu");
    }

    public void shot()
    {
        foreach (GameObject obj in hide) obj.SetActive(false);
        foreach (GameObject obj in show) obj.SetActive(true);
        //NPBinding.MediaLibrary.SaveScreenshotToGallery(screenshotSaved); 
        //NativeToolkit.SaveScreenshot("Unreal Look " + System.DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss"), "Нереальный LOOK");
        StartCoroutine(TakeScreenshotAndSave());
    }

    private void screenshotSaved(string path = "")
    {
        Instantiate(screensaved, transform);
        foreach (GameObject obj in hide) obj.SetActive(true);
        foreach (GameObject obj in show) obj.SetActive(false);
    }

    public void openInstruction()
    {
        Instantiate(instruction, transform);
    }

    private IEnumerator TakeScreenshotAndSave()
    {
        yield return new WaitForEndOfFrame();

        Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.Apply();

        // Save the screenshot to Gallery/Photos
        string album = "Нереальный LOOK", filename = "Unreal LOOK {0}.png";
        //Debug.Log("Permission result: " + NativeGallery.SaveImageToGallery(ss, album, filename));
        string path = NativeGallery.SaveImageToGallery1(ss, album, filename);

        // To avoid memory leaks
        Destroy(ss);
        screenshotSaved();
        //Application.OpenURL(path);
    }
}