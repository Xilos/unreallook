﻿using UnityEngine;

public class GlobalSpawn : MonoBehaviour {
    public GameObject obj;
    private GameObject onScene;
    private bool targetFound = false;
    private GameObject tshirt;

    // Use this for initialization
    void Start ()
    {
        tshirt = GameObject.Find("tshirt-frame");
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (GetComponent<MeshRenderer>().enabled)
        {
            if (!targetFound)
            {
                Destroy(tshirt);
                targetFound = true;
            }
            if (onScene == null)
            {
                onScene = Instantiate(obj, transform);
            }
        }
        else
        {
            if (onScene != null) Destroy(onScene);
        }
    }
}
