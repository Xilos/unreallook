﻿using UnityEngine;

public class space : MonoBehaviour {
    public GameObject host;

	// Use this for initialization
	void Start () {
        transform.parent = null;
    }
	
	// Update is called once per frame
	void Update () {
		if (host == null)
        {
            Destroy(gameObject);
        }
	}
}
