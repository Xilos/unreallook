﻿using UnityEngine;

public class screensaved : MonoBehaviour {
    float start, hiding;
    bool hide = false;
	// Use this for initialization
	void Awake () {
        hiding = start = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        float time = Time.time; ;
        if (time - hiding >= 1) Destroy(gameObject);
        if (time - start >= 5)
        {
            if (!hide) hideIt();
        }
        else
        {
            hiding = time;
        }
	}

    public void hideIt()
    {
        GetComponent<Animator>().Play("hide");
        start -= 5;
        hiding = Time.time;
        hide = true;
    }
}
