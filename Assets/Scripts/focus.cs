﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Vuforia;

public class focus : MonoBehaviour {

    private bool mVuforiaStarted = false;

    void Start()
    {
        GetComponent<VuforiaBehaviour>().enabled = true;
        //VuforiaAbstractConfiguration.Instance.Vuforia.DelayedInitialization = false;
        //VuforiaRuntime.Instance.InitVuforia();
        VuforiaARController vuforia = VuforiaARController.Instance;

        if (vuforia != null)
            vuforia.RegisterVuforiaStartedCallback(StartAfterVuforia);
    }

    private void StartAfterVuforia()
    {
        mVuforiaStarted = true;
        SetAutofocus();
    }

    void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            VuforiaRenderer.Instance.Pause(false);
            // App resumed
            if (mVuforiaStarted)
            {
                // App resumed and vuforia already started
                // but lets start it again...
                SetAutofocus(); // This is done because some android devices lose the auto focus after resume
                // this was a bug in vuforia 4 and 5. I haven't checked 6, but the code is harmless anyway
            }
        }
        else
        {
            VuforiaRenderer.Instance.Pause(true);
            //SceneManager.LoadScene("menu");
        }
    }

    private void SetAutofocus()
    {
        if (CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO))
        {
            Debug.Log("Autofocus set");
        }
        else
        {
            // never actually seen a device that doesn't support this, but just in case
            Debug.Log("this device doesn't support auto focus");
        }
    }
}
