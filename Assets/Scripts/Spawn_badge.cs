﻿using UnityEngine;
using UnityEngine.Video;

public class Spawn_badge : MonoBehaviour {
    public GameObject obj;
    public VideoClip video;
    public Texture texture;
    private GameObject onScene;
    private bool targetFound = false;
    private GameObject tshirt;

	// Use this for initialization
	void Start ()
    {
        tshirt = GameObject.Find("tshirt-frame");
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
		if (GetComponent<MeshRenderer>().enabled)
        {
            if (!targetFound)
            {
                Destroy(tshirt);
                targetFound = true;
            }
            if (onScene == null)
            {
                onScene = Instantiate(obj, transform);
                onScene.GetComponent<VideoPlayer>().clip = video;
                onScene.GetComponent<VideoPlayer>().Play();
                onScene.GetComponent<MeshRenderer>().material.mainTexture = texture;
            }
        }
        else
        {
            if (onScene != null) Destroy(onScene);
        }
	}
}
