﻿using UnityEngine;

public class Raycaster : MonoBehaviour
{
    public static GameObject target = null;

    private RaycastHit hit;
    private Ray ray;
    
    void Update()
    {
        if (Input.touchSupported)
            ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
        else
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit) && (Input.GetMouseButtonDown(0) || Input.touchSupported && Input.GetTouch(0).tapCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended))
        {
            target = hit.transform.gameObject;
        }
        else
        {
            //target = null;
        }
    }
}