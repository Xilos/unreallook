﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class SpawnOrDownload : MonoBehaviour
{
    public string prefabName, title, size;
    [TextArea]
    public string description;
    public bool needsGyroscope;
    public Sprite image;
    public bool isGlobal = false;
    public GameObject cardAsset;
    public int version = 0;

    private GameObject sizeAsset, progressAsset, barAsset, prepareAsset, 
        progressbarAsset, downloadBtn, cancelBtn;

    private GameObject obj;
    private GameObject onScene;
    private GameObject tshirt;
    private GameObject downloadCard;
    private GameObject rayTarget;
    private bool targetFound = false, displayingCard = false, downloading = false;
    private UnityWebRequest www;
    private string url;
    private float barMinPos = -7.5f, barLen = 7.5f;
    private Coroutine coroutine;
    private string assetHash;

    private void Awake()
    {
        //Caching.ClearAllCachedVersions("space");
        //Directory.Delete("space");
    }

    // Start is called before the first frame update
    void Start()
    {
        tshirt = GameObject.Find("tshirt-frame");
        url = "https://unreallook.ru/ar/tshirts/" + prefabName + ".unity3d";
        assetHash = PlayerPrefs.GetString(prefabName, "");
    }
    
    void FixedUpdate()
    {
        //if (www != null && www.downloadProgress < 1) Debug.Log(www.downloadProgress * 100);
        if (GetComponent<MeshRenderer>().enabled)
        {
            if (!targetFound)
            {
                Destroy(tshirt);
                targetFound = true;
            }
            
            /*
            AssetBundle assetBundle = AssetBundle.LoadFromFile(Path.Combine(Path.Combine(Application.persistentDataPath, "bundles"), prefabName + ".unity3d"));

            var fromBundle = assetBundle.LoadAssetWithSubAssetsAsync(prefabName, typeof(GameObject));
            //yield return fromBundle;

            obj = fromBundle.asset as GameObject;
            assetBundle.Unload(false);
            */
            
            if (obj == null)
            {
                if (File.Exists(Path.Combine(Path.Combine(Application.persistentDataPath, "bundles"), prefabName + ".unity3d")) && coroutine == null)
                {
                    coroutine = StartCoroutine(LoadAssetBundle());
                    return;
                }
                if (!displayingCard)
                {
                    //Фигачим карточку на сцену
                    downloadCard = Instantiate(cardAsset, transform);
                    downloadCard.name = downloadCard.name.Substring(0, downloadCard.name.Length - 7);
                    displayingCard = true;

                    //Заполняем статичные тексты на карточке
                    transform.Find("Download asset").Find("Title").gameObject.GetComponent<TextMesh>().text = title;
                    transform.Find("Download asset").Find("Image").GetComponent<SpriteRenderer>().sprite = image;
                    transform.Find("Download asset").Find("Prepare").Find("Description").GetComponent<TextMesh>().text = description;
                    transform.Find("Download asset").Find("Prepare").Find("Gyroscope").gameObject.SetActive(needsGyroscope);

                    //Указываем полный размер загружаемого ассета
                    sizeAsset = transform.Find("Download asset").Find("Size").gameObject;
                    sizeAsset.GetComponent<TextMesh>().text = size + "MB";

                    //Находим объекты с динамически изменяемыми значениями
                    progressAsset = transform.Find("Download asset").Find("Progress bar").Find("background").Find("Progress").gameObject;
                    barAsset = transform.Find("Download asset").Find("Progress bar").Find("background").Find("Bar").gameObject;
                    prepareAsset = transform.Find("Download asset").Find("Prepare").gameObject;
                    progressbarAsset = transform.Find("Download asset").Find("Progress bar").gameObject;
                    downloadBtn = transform.Find("Download asset").Find("Prepare").Find("Download btn").gameObject;
                    cancelBtn = transform.Find("Download asset").Find("Progress bar").Find("Cancel btn").gameObject;

                    if (downloading)
                    {
                        prepareAsset.SetActive(false);
                        progressbarAsset.SetActive(true);
                    }
                }
                if (www == null)
                {
                    if (displayingCard)
                    {
                        /**
                         * При нажатии на кнопку Загрузка убираем её и описание с глаз
                         * Показываем полосу загрузки с процентами и кнопку отмены
                         * */
                        rayTarget = Raycaster.target;
                        if (rayTarget != null && rayTarget == downloadBtn && !downloading)
                        {
                            Raycaster.target = null;
                            downloading = true;
                            prepareAsset.SetActive(false);
                            progressbarAsset.SetActive(true);
                        }
                        if (downloading) coroutine = StartCoroutine(DownloadAssetBundle());
                    }
                }
                else
                {
                    rayTarget = Raycaster.target;
                    if (rayTarget != null && rayTarget == cancelBtn)
                    {
                        Raycaster.target = null;
                        downloading = false;
                        StopCoroutine(coroutine);
                        www = null;
                        Caching.ClearAllCachedVersions(prefabName);

                        prepareAsset.SetActive(true);
                        progressbarAsset.SetActive(false);
                        barAsset.transform.localPosition = new Vector3(barMinPos, barAsset.transform.localPosition.y, barAsset.transform.localPosition.z);
                        progressAsset.GetComponent<TextMesh>().text = "0 %";
                        sizeAsset.GetComponent<TextMesh>().text = size + "MB";
                    }
                    else if (www.downloadProgress < 1)
                    {
                        progressAsset.GetComponent<TextMesh>().text = ((int)(www.downloadProgress * 100)).ToString() + " %";
                        barAsset.transform.localPosition = new Vector3(barMinPos + www.downloadProgress * barLen, barAsset.transform.localPosition.y, barAsset.transform.localPosition.z);
                        string downloaded = (((int)((www.downloadedBytes / 1024.0f / 1024.0f) * 100)) / 100.0f).ToString();
                        sizeAsset.GetComponent<TextMesh>().text = downloaded + "/" + size + "MB";
                    }
                }
                return;
            }
            else
            {
                if (downloadCard != null) Destroy(downloadCard);
                displayingCard = false;
            }

            //www = null;

            if (onScene == null)
            {
                onScene = Instantiate(obj, transform);
            }
        }
        else
        {
            if (onScene != null && !isGlobal) Destroy(onScene);
            if (downloadCard != null) Destroy(downloadCard);
            displayingCard = false;
        }
    }

    IEnumerator DownloadAssetBundle()
    {
        while (!Caching.ready)
            yield return null;

        //CachedAssetBundle cachedAsset = new CachedAssetBundle(prefabName, Hash128.Parse(assetHash));
        www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();
        //PlayerPrefs.SetString(prefabName, cachedAsset.hash.ToString());
        SaveDownloadedAsset(www);


        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log(www.error);
            yield break;
        }
        StartCoroutine(LoadAssetBundle());
    }

    public void SaveDownloadedAsset(UnityWebRequest obj)
    {
        try
        {
            // create the directory if it doesn't already exist
            if (!Directory.Exists(Path.Combine(Application.persistentDataPath, "bundles")))
            {
                Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, "bundles"));
            }

            // write out the file
            string filePath = Path.Combine(Path.Combine(Application.persistentDataPath, "bundles"), prefabName + ".unity3d");
            var bytes = obj.downloadHandler.data;  // initialize the byte string

            File.WriteAllBytes(filePath, bytes);    // write the object out to disk
        }
        catch (Exception e)
        {
            Debug.Log("Couldn't save file: " + System.Environment.NewLine + e);
        }
    }

    IEnumerator LoadAssetBundle()
    {
        //AssetBundle assetBundle = DownloadHandlerAssetBundle.GetContent(www);
        AssetBundle assetBundle = AssetBundle.LoadFromFile(Path.Combine(Path.Combine(Application.persistentDataPath, "bundles"), prefabName + ".unity3d"));

        var fromBundle = assetBundle.LoadAssetWithSubAssetsAsync(prefabName, typeof(GameObject));
        yield return fromBundle;

        obj = fromBundle.asset as GameObject;
        assetBundle.Unload(false);
    }
}
