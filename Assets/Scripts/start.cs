﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class start : MonoBehaviour
{
    private AsyncOperation async;
    float time;
    //public GameObject main;

    private void Awake()
    {
        //DontDestroyOnLoad(Instantiate(main));
    }
    // Use this for initialization
    void Start()
    {
        time = Time.time;
        //if (!vars.first)

        async = SceneManager.LoadSceneAsync("vuforia");
        async.allowSceneActivation = false;
    }

    // Update is called once per frame
    void Update()
    {
        float seconds = Time.time - time;
        if (seconds > 2.5f && async.progress >= 0.9f)
        {
            async.allowSceneActivation = true;
        }
    }
}