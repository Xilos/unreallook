﻿using UnityEngine;

public class hiddenzone : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        Renderer[] rendererComponents = other.transform.gameObject.GetComponentsInChildren<Renderer>(true);
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        Renderer[] rendererComponents = other.transform.gameObject.GetComponentsInChildren<Renderer>(true);
        foreach (Renderer component in rendererComponents)
        {
            component.enabled = true;
        }
    }
}
