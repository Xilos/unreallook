﻿using UnityEngine;

public class spawn : MonoBehaviour {
    public GameObject obj;
    public bool isGlobal = false;
    private GameObject onScene;
    private bool targetFound = false;
    private GameObject tshirt;

	// Use this for initialization
	void Start ()
    {
        tshirt = GameObject.Find("tshirt-frame");
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
		if (GetComponent<MeshRenderer>().enabled)
        {
            if (!targetFound)
            {
                Destroy(tshirt);
                targetFound = true;
            }
            if (onScene == null)
            {
                onScene = Instantiate(obj, transform);
            }
        }
        else
        {
            if (onScene != null && !isGlobal) Destroy(onScene);
        }
	}
}
